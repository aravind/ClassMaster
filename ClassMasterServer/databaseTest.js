/* This script implements basic tests of the database */

// import modules

var client = require('mariasql');

// connect to database
var connection = new client({
  host 		 : 'localhost',
  user       : 'username',
  password   : 'password',
  db   : 'CM'
});

connection.connect(function(err){
	if (err){
		console.log(err);
	}
	else {
		console.log("Database connection successful")
		console.log(" ");
		test1();

		console.log("Closing connection...");
		connection.end();
	}
});

function test1() {
	// test inserting new student into student table 
	connection.query("INSERT INTO Students (Sid, Uname, PWD, did) VALUES (0000000001, 'testUserOne', 'testOnePWD', 'Test device')", function(err, rows){
		console.log("Test 1: Insert new student into Student table:");
		if (err) {
	    	console.log(err);
	    }
	    else {
	  		console.log(rows);
	  		console.log(" ")
	  	}

	  	// print entries in student table
		connection.query("SELECT * FROM Students", function(err, rows) {
			if (err) {
				console.log(err);
			}
			else {
				console.log("Contents of student table:", rows);
				console.log(" ");
			}

			test2();
		});
	});
}

function test2() {
	// attempt to insert a student that already exits into student table
	connection.query("INSERT INTO Students (Sid, Uname, PWD, did) VALUES (0000000001, 'testUserOne', 'testOnePWD', 'Test device')", function(err, rows){
		console.log("Insert duplicate student into student table: this SHOULD produce and error.")
		if (err) {
	    	console.log(err);
		}
	    else {
	  		console.log("Its bad if this prints - insert copy of student-:", rows);
	  		console.log(" ");
	    }

	  	// print entries in student table
		connection.query("SELECT * FROM Students", function(err, rows) {
			console.log("Contents of student table:");
			if (err) {
				console.log(err);
			}
			else {
				console.log( rows);
				console.log(" ");
			}
			clear_and_close();
		});
	});
}

function clear_and_close() {
	// clear all rows in student table
	connection.query("TRUNCATE Students", function(err, rows) {
		if (err)
			console.log(err);
	});
}