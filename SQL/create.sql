DROP DATABASE IF EXISTS CM;
DROP TABLE IF EXISTS Students;
DROP TABLE IF EXISTS Reviews;
DROP TABLE IF EXISTS Classes;
DROP TABLE IF EXISTS Subscribed;
DROP TABLE IF EXISTS Ctaken;
DROP TABLE IF EXISTS tokenID;
DROP TABLE IF EXISTS notifications;

CREATE DATABASE CM;
USE CM;

CREATE TABLE Students
(
Sid int NOT NULL AUTO_INCREMENT,
Uname varchar(255) NOT NULL,
PWD varchar(255) NOT NULL,
PRIMARY KEY(Sid)
);

CREATE TABLE Reviews
(
CRN varchar(20) NOT NULL,
Sid int NOT NULL,
Comment text NOT NULL,
Anony BOOLEAN NOT NULL,
primary key(CRN, Sid)
);

CREATE TABLE Subscribed
(
Sid int NOT NULL,
CRN varchar(20) NOT NULL,
primary key(Sid, CRN)
);

CREATE TABLE Ctaken
(
Sid int NOT NULL,
CRN varchar(20) NOT NULL,
primary key(Sid, CRN)
);

CREATE TABLE Classes
(
CRN varchar(20) NOT NULL,
AvaSpot int,
MaxSpot int,
Cname varchar(255),
AvgStar FLOAT,
TotalStar FLOAT,
primary key(CRN)
);

CREATE TABLE tokenID
(
Sid int NOT NULL,
token varchar(25) NOT NULL,
PRIMARY KEY(token)
);

CREATE TABLE notifications
(
Sid int NOT NULL,
Message varchar(255),
PRIMARY KEY(Sid)
);
