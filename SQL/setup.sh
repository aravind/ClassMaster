#!/usr/bin/expect
spawn sudo mysql

set username "username"
set password "password"

expect "MariaDB"
expect "MariaDB"
expect "MariaDB"
expect "MariaDB"
expect "MariaDB"
send "CREATE USER '$username'@'localhost' IDENTIFIED BY '$password';\n"
expect "MariaDB"
send "GRANT ALL PRIVILEGES ON *.* TO '$username'@'localhost' WITH GRANT OPTION;\n"
expect "MariaDB"
send "source create.sql;\n"
expect "MariaDB"
send "quit;\n"
