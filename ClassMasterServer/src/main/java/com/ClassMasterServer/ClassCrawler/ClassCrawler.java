/**
 * Created by saxenar96 on 9/22/2016.
 */
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.mashape.unirest.request.HttpRequestWithBody;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.htmlunit.HtmlUnitDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;

import org.openqa.selenium.server.browserlaunchers.BrowserStringParser;
import org.simplejavamail.email.Email;
import org.simplejavamail.email.EmailBuilder;
import org.simplejavamail.mailer.Mailer;
import org.simplejavamail.mailer.config.TransportStrategy;
import com.mashape.unirest.http.HttpResponse;

import java.io.File;
import java.io.IOException;
import java.lang.Thread;
import java.util.ArrayList;
import java.sql.*;
import java.util.List;

public class ClassCrawler extends Thread{
	private static final String USERNAME = System.getenv("username");
	private static final String PASSWORD = System.getenv("password");
	private static final String EMAIL_ADDRESS = "saxena12.purdue";
	private static final String URL = "https://selfservice.mypurdue.purdue.edu/prod/bwckschd.p_disp_detail_sched?term_in=201710&crn_in=";
	private static final String CAREER_URL = "https://www.purdue.edu/apps/account/cas/login?service=https://wl.mypurdue.purdue.edu";
		
	static final String JDBC_DRIVER = "com.mysql.jdbc.Driver";
	static final String DB_URL = "jdbc:mysql://localhost:3306/CM";

	static final String USER = "username";
	static final String PASS = "password";

    private static Connection conn;
    private static Statement stmt;

	public ClassCrawler() {}

	public static boolean verifyUser(String username, String password) {
        WebDriver driver = new HtmlUnitDriver();
        driver.get(CAREER_URL);

        // turn off htmlunit warnings
        java.util.logging.Logger.getLogger("com.gargoylesoftware.htmlunit").setLevel(java.util.logging.Level.OFF);
        java.util.logging.Logger.getLogger("org.apache.http").setLevel(java.util.logging.Level.OFF);

        driver.findElement(By.xpath("//*[@id=\"username\"]")).sendKeys(username);
        driver.findElement(By.xpath("//*[@id=\"password\"]")).sendKeys(password);
        driver.findElement(By.xpath("//*[@id=\"fm1\"]/fieldset/div[3]/input[4]")).click();

        String currURL = driver.getCurrentUrl();
        if (currURL.equals(CAREER_URL)) {
            //Invalid username of password
            return false;
        }
        return true;
    }

    public static String getMaxSpot(String crn) {
        WebDriver driver = new HtmlUnitDriver();
        driver.get(URL + crn);
        WebElement max = driver.findElement(By.xpath("/html/body/div[3]/table[1]/tbody/tr[2]/td/table/tbody/tr[2]/td[1]"));
        System.out.println(max.getText());

        try {
            Class.forName(JDBC_DRIVER);

            System.out.println("Connecting to a selected database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");

            System.out.println("Creating statement...");
            Statement stmt = conn.createStatement();
            String sql = "UPDATE Classes" +
                    " SET Classes.MaxSpot = " + max.getText() +
                    " WHERE Classes.CRN = " + crn + ";";
            stmt.executeUpdate(sql);
            System.out.println("Database update complete.");
        }catch (SQLException e) {
        }catch (ClassNotFoundException c) {}

        return crn;
    }

    public static String getClassName(String crn) {
        WebDriver driver = new HtmlUnitDriver();
        driver.get(URL + crn);
        WebElement name = driver.findElement(By.xpath("/html/body/div[3]/table[1]/tbody/tr[1]/th"));
        System.out.println(name.getText());

        try {
            Class.forName(JDBC_DRIVER);

            System.out.println("Connecting to a selected database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");

            System.out.println("Creating statement...");
            Statement stmt = conn.createStatement();
            String sql = "UPDATE Classes" +
                    " SET Classes.Cname = \"" + name.getText() + "\"" +
                    " WHERE Classes.CRN = " + crn + ";";
            System.out.println(sql);
            stmt.executeUpdate(sql);
            System.out.println("Database update complete.");
        }catch (SQLException e) {
        }catch (ClassNotFoundException c) {}

        return crn;
    }

	public static ArrayList<String> getAvailability() {
	    ArrayList<String> crns = new ArrayList<String>();

	    try {
            Class.forName(JDBC_DRIVER);

            System.out.println("Connecting to a selected database...");
            conn = DriverManager.getConnection(DB_URL, USER, PASS);
            System.out.println("Connected database successfully...");

            stmt = conn.createStatement();

            String sql = "SELECT * FROM Classes;";
            ResultSet rs = stmt.executeQuery(sql);
            System.out.println(rs.toString());

            while(rs.next()) {
                int crn = rs.getInt("CRN");
                getAvailableSeats(Integer.toString(crn), crns, rs.getInt("AvaSpot"));
            }
            System.out.println("All Info gathered");
        } catch (SQLException se) {
            //Handle errors for JDBC
            se.printStackTrace();
        }catch(Exception e) {
            //Handle errors for Class.forName;
            e.printStackTrace();
        }finally{
            try {
                if (stmt != null)
                    conn.close();
            }catch(SQLException se) {
                se.printStackTrace();
            }//end finally try
        }//end try
        return crns;
    }

	public static void getAvailableSeats(String crn, ArrayList<String> crns, int AvaSpots) {
		WebDriver driver = new HtmlUnitDriver();

        driver.get(URL + crn);
        WebElement remaining = null;
        try {
            remaining = driver.findElement(By.xpath("/html/body/div[3]/table[1]/tbody/tr[2]/td/table/tbody/tr[4]/td[3]"));
        } catch(NoSuchElementException e) {
            try {
                remaining = driver.findElement(By.xpath("/html/body/div[3]/table[1]/tbody/tr[2]/td/table/tbody/tr[2]/td[3]"));
            } catch (Exception e1) {
                try {
                    Thread.sleep(60000);
                } catch(InterruptedException e3) {
                }
            }
        }
        int spaces = Integer.valueOf(remaining.getText());
        System.out.println(spaces + " -> " + crn);

        int spots = spaces;

        System.out.println("Spots: " + spots + " | AvaSpot: " + AvaSpots);
        if (spots != AvaSpots) {
            updateDB(crn, spots);
            if (spots > 0)
                crns.add(crn);
        } else {
            System.out.println("Same spots for " + crn);
        }
	}   

	public static void updateDB(String crn, int spots) {
        try {
            System.out.println("Creating statement...");
            Statement stmt = conn.createStatement();
            String sql = "UPDATE Classes" +
                    " SET Classes.AvaSpot = " + spots +
                    " WHERE Classes.CRN = " + crn + ";";
            stmt.executeUpdate(sql);
            System.out.println("Database update complete.");
        }catch (SQLException e) {}
	}

    public static void main(String[] args) {
        if (args.length == 2 && args[0].equals("getName")) {
            String name = getClassName(args[1]);
            System.out.println(name);
        }
        else if (args.length == 2 && args[0].equals("getMaxSpot")) {
            String crn = getMaxSpot(args[1]);
            System.out.println(crn);
        }
        else if (args.length == 2) {
            boolean val = verifyUser(args[0],args[1]);
            System.out.println(val);
        }
        else {
            ArrayList<String> crns;
            while(true) {
                crns = getAvailability();
                //crns.add("1235");

                for (int i = 0; i < crns.size(); i++) {
                    try {
                        HttpResponse<JsonNode> jsonResponse = Unirest.post("http://localhost:3000/UpdateCRN/" + crns.get(i))
                                .asJson();
                    } catch (UnirestException e) {
                        e.printStackTrace();
                    }
                }
                try {
                    sleep(3000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }
	}
}

