// import modules
var express = require("express");
var client = require('mariasql');
var bodyParser = require('body-parser');

/* Database connection info */
var connection = new client({
  host     : 'localhost',
  user       : 'username',
  password   : 'password',
  db       : 'CM'
});

/* connect to database */
connection.connect(function(err){
  if (err){
    console.log(err);
  }
  else {
    console.log("Database connection successful")
  }
});

var app = express();
app.use(bodyParser.json());

var exec = require('child_process').exec;

/* spawn web crawler */
console.log("Spawn web crawler");
var webCrawler = exec('mvn test');

/* display the tables in CM to ensure database is up to date */
connection.query('SHOW TABLES', function(err, rows) {
  if (err)
    throw err;
  console.dir(rows);
});

// start listening on port 3000
app.listen(3000, function(err) {
  if (err) {
    console.log("listen error");
    err.throw();
  }
  console.log("Server started.");
});

/**
 * Test request to test connection
 */
app.get('/', function (req, res) {
  res.send("This is a test");
});


/**
 * Handle login requests
 Get rid of signup and in case of login of a non-existing user,
 validate the new user through web crawler and add if validated
 */
app.post('/Login', function(req, res) {
  console.log("Get login request");

  /* callback function to handle reponse */
  var callback = function(result) {
    if (result == -1) {
      res.json({"response": "login failed", "res": "false"});
    }
    else {
      res.json({"response": "login successful", "res":"true", "sid" : result});
    }
  }

  /* check for undefined or missing input */
  if (req.body.username == undefined || req.body.password == undefined) {
    console.log("login: undefined args");
    callback(-1);
  }
  else {
    login(req.body.username, req.body.password, callback);
  }
});


/**
 * Handle subscribe requests
 */
app.post('/subscribe', function(req, res) {
  console.log("Post subscribe request");

  /* callback function to handle response */
  var callback = function(result) {
    if (result == -1) {
      res.json({"response": "subscribe failed", "res": "false"});
    }
    else {
      res.json({"response": "subscribe successful", "res":"true"});
    }
  };

  /* handle undefined or missing input */
  if (req.body.CRN == undefined || req.body.studentid == undefined) {
    console.log("subscribe: undefined input");
    callback(-1);
  }
  else {
    subscribe(req.body.CRN, req.body.studentid, callback);
  }
});

/**
 * Handle unsubscribe requests
 */
app.post('/unsubscribe', function(req, res) {
  console.log("Post unsubscribe request");

  /* callback to handle response */
  var callback = function(result) {
    if (result == -1) {
      res.json({"response": "unsubscribe failed", "res": "false"});
    }
    else {
      res.json({"response": "unsubscribe successful", "res":"true"});
    }
  };

  /* handle missing or undefined input */
  if (req.body.CRN == undefined || req.body.studentid == undefined) {
    console.log("unsubscribe: undefined input");
    callback(-1);
  }
  else {
    unsubscribe(req.body.CRN, req.body.studentid, callback);
  }
});


/**
 * Handle subscription requests
 */
app.post('/subscriptionList', function(req, res) {
  console.log("Get subscriptionList request");

  /* callback to handle reponse */
  var callback = function(result) {
    res.json({"results": result});
  };

  /* handle undefined input */
  if (req.body.studentid == undefined) {
    console.log("subsctionList: undefined input");
    callback(null);
  }
  else {
    getSubsciptionList(req.body.studentid, callback);
  }
});

/**
 * Handle available spots requests
 */
app.post('/AvaSpot', function(req, res) {
  console.log("Get Available Spot request");

  /* callback to handle reponse */
  var callback = function(result) {
    res.json({"results": result});
  }

  /* handle undefined input */
  if(req.body.CRN == undefined) {
    console.log("retrieveAvaSpot: undefined input");
    callback(null);
  }
  else {
    retrieveAvaSpot(req.body.CRN, callback);
  }
});

/**
 * Handle max spots requests
 */
app.post('/MaxSpot', function(req, res) {
  console.log("Get MAX Spot request");

  /* callback to handle reponse */
  var callback = function(result) {
    res.json({"results": result});
  }

  /* handle undefined input */
  if(req.body.CRN == undefined) {
    console.log("retrieveMaxSpot: undefined input");
    callback(null);
  }
  else {
    retrieveAvaSpot(req.body.CRN, callback);
  }
});

/**
 * Store a new review for a class
 */
app.post('/Review', function(req, res) {
  console.log("new review request");

  /* callback to handle reponse */
  var callback = function(result) {
    if (result == -1) {
      res.json({ "response": "review failed", "res": "false" });
    }
    else {
      res.json({ "response" : "review successful", "res": "true" });
    }
  };

  /* handle missing input */
  if (req.body.CRN == undefined || req.body.Sid == undefined || req.body.Comment == undefined){
    console.log("Review: undefined input");
    callback(-1);
  }
  else {
    /* assume anony to be false unless specified */
    var anony = false;
    if (req.body.anony != undefined) {
      anony = req.body.anony;
    }
    storeReview(req.body.CRN, req.body.Sid, req.body.comment, anony, callback);
  }
});


/**
 * Return the inform for a class with the given CRN
 */
app.post('/GetClassInfo', function(req, res) {
  console.log("get class info request");

  /* callback to handle response */
  var callback = function(result) {
    /* respone format: availableSpots, maxSpots, cName, avgStar, totalStar */
    if (result == -1) {
      res.json({ "res": "false", "result" : null });
    }
    else {
      res.json({ "res": "true", "result" : result });
    }
  }

  /* handle missing input */
  if (req.body.CRN == undefined) {
    console.log("GetClassInfo: undefined input");
    callback(-1);
  }
  else {
    retrieveClassInfo(req.body.CRN, callback);
  }
});


/**
 * Returns all of the reviews for the class withe the specified CRN
 */
app.post('/GetReviewsForClass', function(req, res) {
  console.log("get reviews request");

  var callback = function(result) {
    if (result == -1) {
      res.json({ "res": "false", "reviews" : null });
    }
    else {
      res.json({ "res" : "true", "reviews" : result });
    }
  };

  /* handle missing input */
  if (req.body.CRN == undefined) {
    console.log("Get Reviews for Class: undefined input");
    callback(-1);
  }
  else {
    retrieveReviews(req.body.CRN, callback);
  }
});

/**
 * Returns all of the classes taken for the student with the specified Sid
 */
app.post('/retrieveCtaken', function(req, res) {
  console.log("Get class taken request");

  /* callback to handle reponse */
  var callback = function(result) {
    res.json({"results": result});
  };

  /* handle undefined input */
  if (req.body.studentid == undefined) {
    console.log("retrieveCtaken: undefined input");
    callback(null);
  }
  else {
    retrieveCtaken(req.body.studentid, callback);
  }
});


/**
 * Used by the web crawler to pass list of updated classes with seats available
 * for notification purposes
 */
app.post('/UpdateCRN/:crn', function(req, res) {
  console.log("update CRNs request");

  /* callback to handle response */
  var callback = function(result) {
    if (result == -1) {
      res.json({ "res" : "false" });
    }
    else {
      res.json({ "res" : "true" });
    }
  };

  /* handle undefined input */
  if (req.params.crn == undefined){
    console.log("updateCRNs: undefined input");
    callback(-1);
  }
  else {
    checkAfterUpate(req.params.crn, callback);
  }

});

/**
 * returns a list of stored messages for the user
 */
app.post('/checkForNotifications', function(req, res) {
  console.log("check for notifications request");

  /* callback to handle response */
  var callback = function(result) {
    if (result == -1) {
      res.json({ "res" : "false" });
    }
    else {
      res.json({ "res" : "true", "result" : result });
    }
  };

  /* handle missing input */
  if (req.body.Sid == undefined) {
    console.log("checkForNotifications undefined input");
    callback(-1);
  }
  else {
    checkForNotifications(req.body.Sid, callback);
  }
});


/**
 * Update average and total stars for a class
 */
app.post('/UpdateStar', function(req, res) {
  console.log("update Star request");

  /* callback to handle response */
  var callback = function(result) {
    if (result == -1) {
      res.json({ "res" : "false" });
    }
    else {
      res.json({ "res" : "true" });
    }
  };

  /* handle undefined input */
  if (req.body.CRN == undefined || req.body.newStar == undefined){
    console.log("updateCRNs: undefined input");
  } else {
    updateStar(req.body.CRN, req.body.newStar, callback);
  }

});

/**
 * Returns the current star rating of the specified class as float
 */
app.post('/getCurrentRating', function(req, res) {
  console.log("get current star request");

  /* callback to handle response */
  var callback = function(result) {
    if (result == -1) {
      res.json({ "res" : "false" });
    }
    else {
      res.json({ "res" : "true", "result" : result });
    }
  }

  /* handle missing input */
  if (req.body.CRN == undefined) {
    console.log("getCurrentStar: undefined input");
    callback(-1);
  }
  else {
    getCurrentStarRating(CRN, callback);
  }
});

app.post('/getCRNFromName', function(req, res) {
	console.log("get crn from name request");

	var callback = function(result) {
		if (result == -1) {
			res.json({"res" : "false"});
		}
		else {
			res.json({"res" : "true", "crn" : result});
		}
	}

	/* handle undefined input */
	if (req.body.name == undefined) {
		console.log("get crn from name: indefined input");
		callback(-1);
	} 
	else {
		getCRNFromName(req.body.name, callback);
	} 
});

/**
 * Sign a new user up in the system
 * Adds a new entry in the student table
 */
function signup(username, password, callback) {
  console.log("Signup: ", username, password);

  // +++check format of all input+++

  var insert = "INSERT INTO Students (Uname, PWD) VALUES ('" + username + "', '" + password + "' )";
  // insert into student table - if error occurs, username already exists
  var result = connection.query(insert, function(err, rows, res){
    if (err) {
      console.log("Username already exits!", err);
      return callback(-1);
    }
    else {
      //console.log(rows);
      console.log("Signup Successful")
      // successful signup
      login(username, password, callback);
    }
  });
}

/**
 * Login a user with the given username and password by ensuring if they exist in the student table
 */
function login(username, password, callback) {
  console.log("login: ", username, password);

  // Check format of input

  var select = "SELECT * FROM Students WHERE Uname LIKE '" + username + "' AND PWD LIKE '" + password + "'";

  connection.query(select, function(err, rows){
    if (err) {
      console.log("Login Failed", err);
      return callback(-1);
    }
    else {
      // check that there is one and only one match
      if (rows.length == 1) {
        console.log("Login Successful");
        // successful login
        console.log("returned to callback: ", rows[0].Sid);
        return callback(rows[0].Sid);
      }
      else
      {
        console.log("Login Failed, rows.length: " + rows.length)
        if (rows.length == 0) {
          /* validate new user through web crawler */
          var validate = exec("mvn test -Dexec.args=\"" + username + " " + password + "\"");
          validate.stdout.on('data', function(data) {
            //console.log("Validate output: " + data);
            var valid = data;

            if (valid.indexOf("true") != -1) {
              console.log("validation successful");
              /* add new use */
              signup(username, password, callback);
            }
            else if (valid.indexOf("false") != -1) {
              console.log("validation failed");
              return callback(-1);
            }

          });
        }
      }
    }
  });
}

/*
 * Subscribe the student to the specified class
 */
function subscribe(CRN, studentid, callback) {
  console.log("Subscribe: ", CRN);

  valid = true; //false;
  // check that CRN is valid

  if (!valid) {
    console.log("Invalid CRN");
    return callback(-1);
  }
  //check student id
  var select = "SELECT * FROM Students WHERE Sid LIKE '" + studentid + "'";
  connection.query(select, function(err, rows){
    if(rows.length == 0){
      console.log("Error subscribing to class, Invalid studentid");
      return callback(-1);
    } else{
      // add entry in Subscribe table
      var insert = "INSERT INTO Subscribed (Sid, CRN) VALUES ('" + studentid + "', '" + CRN +"')";

      connection.query(insert, function(err, rows){
        if (err) {
          console.log("Error subscibing to class", CRN, err);
          return callback(-1);
        }
        else {
          //console.log(rows);
          console.log("Subscibe Successful")
          // successful signup
          return callback(1);
        }
      });

      /* attempt to insert class into table */
      var insert2 = "INSERT INTO Classes (CRN, MaxSpot, AvgStar, TotalStar) VALUES ('" + CRN + "', '0', '1.0', '1.0')";

      connection.query(insert2, function(err, row) {
        if (err) {
          console.log("If this is a duplicate entry error, then its not a problem: ", err);
        }
        else {
          console.log("Class " + CRN + " added to classes table.");

          /* get info for classes */
          var getName = exec("mvn test -Dexec.args=\"getName " + CRN + "\"");
          getName.stdout.on('data', function(data) {
            //console.log("getName output: " + data);
          });
          var getMaxSpots = exec("mvn test -Dexec.args=\"getMaxSpot " + CRN + "\"");
          getMaxSpots.stdout.on('data', function(data) {
            //console.log("getMaxSpots output: " + data);
          });
        }
      });
    }
  });

}

/**
 * Unsubscribe the student from the specified class
 */
function unsubscribe(CRN, studentid, callback) {
  console.log("Unsubscribe: ", CRN);

  // ensure that CRN is valid
  var select = "SELECT * FROM Subscribed WHERE Sid='" + studentid + "' AND CRN='" + CRN + "'";
  connection.query(select, function(err, rows){
    if (err) {
      console.log("Error unsubscribing to class", CRN, err);
      return callback(-1);
    }
    else {
      if(rows.length == 0){
        console.log("It does not exist in the subscription list");
        return callback(-1);
      } else{
        // delete entry from Subscribed table
        var deletion = "DELETE FROM Subscribed WHERE Sid='" + studentid + "' AND CRN='" + CRN + "'";
        connection.query(deletion, function(err, rows){
          if (err) {
            console.log("Error unsubscibing to class", CRN, err);
            return callback(-1);
          }
          else {
            //console.log(rows);
            console.log("Unsubscibe Successful", rows.length);
            // successful signup
            return callback(1);
          }
        });
      }

    }
  });
}


/**
 * Returns the list of CRNs that the student is subscribed to
 */
function getSubsciptionList(studentid, callback) {
  var search = "SELECT * FROM Subscribed INNER JOIN Classes WHERE Subscribed.CRN = Classes.CRN AND Sid = '" + studentid + "'";
  connection.query(search, function(err, rows){
    if (err) {
      console.log("Get subscriptions Failed", err);
      return callback(-1);
    }
    else {
      return callback(rows);
    }
  });
}


/**
 * Stores the Token id in the tokenid table
 */
function storeTokenID(studentid, tokenID, callback) {
  var insert = "INSERT INTO tokenID (Sid, token) VALUES ('" + studentid + "', '" + tokenID +"')";

  connection.query(insert, function(err, rows){
    if (err) {
      console.log("Error storing tokenID to class", err);
      return callback(-1);
    }
    else {
      //console.log(rows);
      console.log("Token stored successfully")
      // successful signup
      return callback(1);
    }
  });
}

/**
 * Retrieves the token id for the specified user
 */
function retrieveTokenID(studentid, callback) {
  var select = "SELECT * FROM tokenID WHERE SID LIKE '" + studentid + "'";

  connection.query(select, function(err, rows){
    if (err) {
      console.log("Token retrieval failed", err);
      return callback(-1);
    }
    else {
      console.log("Token reteived successfully");
      return callback(rows);
    }
  });
}

/**
 * Stores a student review of a class in the Reviews table
 */
function storeReview(CRN, Sid, Comment, Anony, callback) {
  var insert = "INSERT INTO Reviews (CRN, Sid, Comment, Anony) VALUES ('" + CRN + "', '" + Sid + "', '" + Comment + "', '" + Anony + "')";

  connection.query(insert, function(err, rows) {
    if (err) {
      console.log("storeReview error", err);
      return callback(-1);
    }
    else {
      console.log("review stored successfully");
      return callback(1);
    }
  });
}

/**
 * Retrieves the number of available spots for the specified class
 */
function retrieveAvaSpot(CRN, callback) {
  var select = "SELECT AvaSpot FROM Classes WHERE CRN LIKE '"+  CRN +"'";
  connection.query(select, function(err, rows){
    if (err) {
      console.log("AvaSpot retrieval failed", err);
      return callback(-1);
    }
    else {
      console.log("AvaSpot retrieved successfully");
      return callback(rows);
    }
  });
}

/**
 * Retrieves the max spots in the specified class
 */
function retrieveMaxSpot(CRN, callback) {
  var select = "SELECT MaxSpot FROM Classes WHERE CRN LIKE '"+  CRN +"'";
  connection.query(select, function(err, rows){
    if (err) {
      console.log("MaxSpot retrieval failed", err);
      return callback(-1);
    }
    else {
      console.log("MaxSpot retrieved successfully");
      return callback(rows);
    }
  });
}

/**
 * Retrieves all class info for the specified class
 */
function retrieveClassInfo(CRN, callback) {
  var select = "SELECT * FROM Classes WHERE CRN LIKE '" + CRN + "'";

  /* query the database */
  connection.query(select, function(err, rows) {
    if (err) {
      console.log("retieve class info error", error);
      return callback(-1);
    }
    else {
      console.log("retieve class info successful");
      return callback(rows[0]);
    }
  });
}

/**
 * Retrieves reviews for the specified class
 */
function retrieveReviews(CRN, callback) {
  var select = "SELECT * FROM Reviews WHERE CRN LIKE '" + CRN + "'";

  /* query database */
  connection.query(select, function(err, rows) {
    if (err) {
      console.log("retrieve reviews error", err);
      return callback(-1);
    }
    else {
      console.log("retrieve reviews successful");
      return callback(rows);
    }
  });
}

/**
 * Retrieves the list of classes taken for the specified user from the ClassesTaken table
 */
function retrieveCtaken(Sid, callback) {
  var select = "SELECT CRN FROM Ctaken WHERE Sid LIKE '" + Sid + "'";

  /* query database */
  connection.query(select, function(err, rows) {
    if (err) {
      console.log("retrieve class taken error", err);
      return callback(-1);
    }
    else {
      console.log("retrieve class taken successful");
      return callback(rows);
    }
  });
}

/**
 * checks the updated CRNs for available spots and adds notifications to
 * table for subscribed users.
 */
function checkAfterUpate(CRN, callback) {
  /* for each crn in list, check things */
  console.log("CRN: " + CRN);

  var select = "SELECT * FROM Subscribed WHERE CRN LIKE '" + CRN  + "'";
  connection.query(select, function(err, rows) {
    if (err) {
      console.log("check after update: error getting list of subscribers for CRN", CRN, err);
      return callback(-1);
    }
    else {
      console.log("check after update: retrieval of subscriber list successful for CRN", CRN);

      /* add notification for each subscribed user */
      for (var i = 0; i < rows.length; i++) {
        var Sid = rows[i].Sid;
        var Message = "The class " + CRN + " has seats available."
        var insert = "INSERT INTO notifications (Sid, Message) VALUES ('" + Sid + "', '" + Message + "')";
        connection.query(insert, function(err, rows) {
          if (err) {
            console.log("check after update: error adding notification for student " + Sid + ".", err);
          }
          else {
            console.log("notification added for student " + Sid + " for class " + CRN);
          }
        });
        return callback(1);
      }
    }
  });
}

/**
 * Retrieves list of waiting messages from user and then deletes
 * from database.
 */
function checkForNotifications(Sid, callback) {
  var select = "SELECT * FROM notifications WHERE Sid LIKE '" + Sid + "'";

  connection.query(select, function(err, rows) {
    if (err) {
      console.log("checkfornotifications error", err);
      return callback(-1);
    }
    else {
      console.log("retreve notifications successful");

      var deletion = "DELETE FROM notifications WHERE Sid='" + Sid + "'";

      connection.query(deletion, function(err, rows) {
        if (err) {
          console.log("checkfornotifications deletion error", err);
        }
        else {
          console.log("check for notifications deleteion successful");
        }
      });

      return callback(rows);
    }
  });
}

/**
 * Update average and total stars for a specified class
 */
function updateStar(CRN, newStar, callback) {
  var AvgStar = "SELECT AvgStar FROM Classes WHERE CRN LIKE '" + CRN + "'";
  var AvgNum = 0;
  var TotNum = 0;
  /* query database */
  connection.query(AvgStar, function(err, rows) {
    if (err) {
      console.log("retrieve AvgStar error", err);
      return callback(-1);
    }
    else {
      console.log("retrieve AvgStar successful");
      if (rows.length == 0) {
        return callback(-1);
      }
      AvgNum = Number(rows[0].AvgStar);

      var TotalStar = "SELECT TotalStar FROM Classes WHERE CRN LIKE '" + CRN + "'";
      connection.query(TotalStar, function(err, rows) {
        if (err) {
          console.log("retrieve TotalStar error", err);
          return callback(-1);
        }
        else {
          console.log("retrieve TotalStar successful");
          TotNum = Number(rows[0].TotalStar);

          var times = 0;
          if (AvgNum != 0){
            times = TotNum/AvgNum;
          }
          var newTotalStar = TotNum + Number(newStar);
          var newAvgStar = newTotalStar/(times + 1);

          var select = "Update Classes set AvgStar = '" +newAvgStar+"', TotalStar = '"+ newTotalStar +"' WHERE CRN LIKE '" + CRN + "'";
          connection.query(select, function(err, rows) {
            if (err) {
              console.log("Update stars error", err);
              return callback(-1);
            }
            else {
              console.log("Update stars successful");
              return callback(rows);
            }
          });
        }
      });
    }
  });
}

function getCurrentStarRating(CRN, callback) {
  var select = "SELECT * FROM Classses WHERE CRN LIKE '" + CRN + "'";

  connection.query(select, function(err, rows) {
    if (err) {
      console.log("getCurrentStarRating error", err);
      return callback(-1);
    }
    else {
      console.log("getCurrentStarRating Successful");
      return callback(rows[0].AvgStar);
    }
  });
}

function getCRNFromName(Cname, callback) {
	var select = "SELECT * FROM Classes WHERE Cname LIKE '" + Cname + "'";
	console.log("get CRN triggered");

	connection.query(select, function(err, rows) {
		if (err) {
			console.log("getCRNFromName error", err);
		}
		else {
			console.log("getCRNFromName successful");
			return callback(rows[0].CRN);
		}
	}); 
};

connection.end();
